public class Board {
    private Square[][] tictactoeBoard; //PT2 - 1

	//PT 2 - 2
    public Board() {
        tictactoeBoard = new Square[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                tictactoeBoard[i][j] = Square.BLANK;
            }
        }
    }

    //PT2 - 3
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("  0 1 2\n");
		for (int i = 0; i < 3; i++) {
			sb.append(i).append(" ");
			for (int j = 0; j < 3; j++) {
				sb.append(tictactoeBoard[i][j]).append(" ");
			}
			sb.append("\n");
		}
		return sb.toString();
	}
	
	/*PT2 - 3
	Previous version of the toString method that was 
	printing the board incorrecntly.
	Just for reference for myself for later.
	
	public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                sb.append(tictactoeBoard[i][j].toString());
                if (j < 2) {
                    sb.append("|");
                }
            }
            sb.append("\n");
            if (i < 2) {
                sb.append("-----\n");
            }
        }
        return sb.toString();
    }
	*/


	//PT2 - 4
    public boolean placeToken(int row, int col, Square playerToken) {
        if (row < 0 || row > 2 || col < 0 || col > 2) {
            return false;
        }
        if (tictactoeBoard[row][col] != Square.BLANK) {
            return false;
        }
        tictactoeBoard[row][col] = playerToken;
        return true;
    }

	//PT2 - 5
    public boolean checkIfFull() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (tictactoeBoard[i][j] == Square.BLANK) {
                    return false;
                }
            }
        }
        return true;
    }
	
	//PT2 - 6
    private boolean checkIfWinningHorizontal(Square playerToken) {
        for (int i = 0; i < 3; i++) {
            if (tictactoeBoard[i][0] == playerToken && tictactoeBoard[i][1] == playerToken && tictactoeBoard[i][2] == playerToken) {
                return true;
            }
        }
        return false;
    }
	
	//PT2 - 7
    private boolean checkIfWinningVertical(Square playerToken) {
        for (int i = 0; i < 3; i++) {
            if (tictactoeBoard[0][i] == playerToken && tictactoeBoard[1][i] == playerToken && tictactoeBoard[2][i] == playerToken) {
                return true;
            }
        }
        return false;
    }
	
	//PT2 - 8
    public boolean checkIfWinning(Square playerToken) {
        if (checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken)) {
            return true;
        }
        return false;
    }
}
