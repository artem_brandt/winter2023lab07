import java.util.Scanner;

public class TicTacToeGame {
    public static void main(String[] args) {
        System.out.println("Welcome to the Tic Tac Toe game!");

        Board board = new Board();

        boolean gameOver = false;
        int player = 1;
        Square playerToken = Square.X;

        Scanner scanner = new Scanner(System.in);

        while (!gameOver) {
            System.out.println(board);

            if (player == 1) {
                playerToken = Square.X;
            } else {
                playerToken = Square.O;
            }

            System.out.println("Player: " + player + ", it's your turn");
            System.out.print("Enter row (betwen 1 and 3): ");
            int row = scanner.nextInt() - 1;
            System.out.print("Enter column (betwen 1 and 3): ");
            int col = scanner.nextInt() - 1;

            while (!board.placeToken(row, col, playerToken)) {
                System.out.println("Invalid input. Please try again.");
                System.out.print("Enter row (1-3): ");
                row = scanner.nextInt() - 1;
                System.out.print("Enter column (1-3): ");
                col = scanner.nextInt() - 1;
            }

            if (board.checkIfWinning(playerToken)) {
                System.out.println("Player " + player + " wins! Congratulations.");
                gameOver = true;
            } else if (board.checkIfFull()) {
                System.out.println("It's a tie! Replay the game for one more round.");
                gameOver = true;
            } else {
                player++;
                if (player > 2) {
                    player = 1;
                }
            }
        }
    }
}
